
class Play:
    def __init__(self, data):
        self.name = data["name"]
        self.type = data["type"]


class Invoice:
    def __init__(self, data):
        self.customer = data["customer"]
        self.performances = [Performance(**perf) for perf in data["performances"]]


class Performance:
    def __init__(self, play_id, audience):
        self.play_id = play_id
        self.audience = audience
